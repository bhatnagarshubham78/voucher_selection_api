# Data related to Swagger API

docs_title = "Voucher Selection API"
version = "1.0.0"
openapi_url = "/customer-voucher/openapi.json"
docs_url = "/customer-voucher/apidoc"
description = """
Voucher Generation API
For given customer object return voucher amount based on most used voucher in corresponding customer segment.

## Parameters:
* customer_id: id of the customer
* country_code: customer’s country ('Peru')
* last_order_ts: timestamp of the last order placed by a customer
* first_order_ts: timestamp f the first order placed by a customer
* total_orders: total orders placed by a customer
* segment_name: segment a customer belongs to ['recency_segment, frequent_segment]


## Raises:
* HTTPException: status_code=500 -> Undefined parameters, unable to read voucher amount files
* HTTPException: status_code=204 -> No voucher found for the customer
* HTTPException: status_code=422 -> Invalid API parameters data-type
* Success: status_code=200 (Ok) -> float: selected voucher amount for the customer

## Returns:
* float: selected voucher amount for the customer
Examples:
${api_url}/customer-voucher?customer_id=2&country_code=peru&last_order_ts=2018-05-03 00:00:00&first_order_ts=2017-05-03 00:00:00&total_orders=30&segment_name=recency_segment
    """

tags_metadata = [
    {
        "name": "Health",
        "description": "Check health of the app",
    },
    {
        "name": "VoucherSelection",
        "description": "Get selected voucher for customer",
    },
]
