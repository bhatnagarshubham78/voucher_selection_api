import os
import sys
from pathlib import Path
import logging

# static logging attributes
log_format = '%(asctime)s | %(levelname)s | %(filename)s | %(message)s'
log_level = logging.INFO

# Logging level
logging.basicConfig(stream=sys.stdout, level=log_level, format=log_format)

home_dir = os.path.abspath(os.path.join(__file__, "../../"))
data_dir = home_dir + "/data/"
log_dir = home_dir + "/logs/"
config_dir = home_dir + "/config/"
voucher_data_file = 'data.parquet.gzip'
total_order_quantile_filter = 0.86

frequency_segment_dict = {
    range(0, 5): "0-4",
    range(5, 14): "5-13",
    range(14, 38): "14-37"
}

segment_names = ['recency_segment', 'frequent_segment']

Path(log_dir).mkdir(parents=True, exist_ok=True)
Path(data_dir).mkdir(parents=True, exist_ok=True)

country_code = 'Peru'
voucher_amount_recency_file = 'voucher_amount_recency.json'
voucher_amount_frequency_file = 'voucher_amount_frequency.json'
