import sys
from fastapi import FastAPI

sys.path.append('../')

from src.routes import ping, voucher_selection
from config import swagger_doc
from src.constants import application_constants

app = FastAPI(
    title=swagger_doc.docs_title,
    description=swagger_doc.description,
    version=swagger_doc.version,
    openapi_url=swagger_doc.openapi_url,
    docs_url=swagger_doc.docs_url,
    openapi_tags=swagger_doc.tags_metadata
)

# register all the routers
app.include_router(ping.router, prefix=application_constants.PREFIX_APP_URL, tags=["Health"])
app.include_router(voucher_selection.router, tags=["VoucherSelection"])
