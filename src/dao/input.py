from enum import Enum
from dataclasses import dataclass


# class to map input request into python dict
@dataclass
class InputDao():
    customer_id: int
    country_code: str
    last_order_ts: str
    first_order_ts: str
    total_orders: float
    segment_name: str


class SegmentName(str, Enum):
    recency_segment: str = "recency_segment"
    frequent_segment: str = "frequent_segment"
