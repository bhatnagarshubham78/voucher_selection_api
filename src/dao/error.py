from dataclasses import dataclass


# standard error dict format
@dataclass
class ErrorDao():
    status: str
    message: str
