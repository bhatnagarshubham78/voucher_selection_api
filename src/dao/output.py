from dataclasses import dataclass


# standard output dict format
@dataclass
class OutputDao():
    voucher_amount: float
