import json
import sys

import numpy as np
import pandas as pd

sys.path.append('../')
from config.main_config import total_order_quantile_filter, logging, country_code, \
    voucher_amount_frequency_file, voucher_amount_recency_file
from config.main_config import data_dir, voucher_data_file
from utils.helper_functions import get_recency_segment, get_frequency_segment

pd.set_option('mode.chained_assignment', None)


class VoucherGeneration:
    """
    Class for voucher generation for customers
    """
    def __init__(self):
        self.voucher_amount_frequency = None
        self.voucher_amount_recency = None

    def read_voucher_data(self):
        """
        Reads voucher data into pandas dataframe
        """
        voucher_df = pd.read_parquet(data_dir + '/' + voucher_data_file)
        return voucher_df

    def pre_process_voucher_data(self, voucher_df):
        """
        Pre-processes voucher data as in data type conversions and null records removal
        :param voucher_df:
        :return: dataframe: pre-processed dataframe
        """
        voucher_df.drop_duplicates(inplace=True)
        voucher_df['total_orders'] = pd.to_numeric(voucher_df['total_orders'], errors='coerce')
        voucher_df['total_orders'] = voucher_df['total_orders'].replace(np.nan, 0, regex=True)
        voucher_df['total_orders'] = voucher_df['total_orders'].astype(int)
        voucher_df['voucher_amount'] = voucher_df['voucher_amount'].replace(np.nan, 0, regex=True)
        voucher_df['timestamp'] = pd.to_datetime(voucher_df.timestamp, format='%Y-%m-%d %H:%M:%S',
                                                 errors='coerce')
        voucher_df['last_order_ts'] = pd.to_datetime(voucher_df.last_order_ts, format='%Y-%m-%d %H:%M:%S',
                                                     errors='coerce')
        voucher_df.dropna(how='any', axis=0, inplace=True)

        return voucher_df

    def outlier_removal(self, voucher_df):
        """
        Removes outlier from voucher data
        :param voucher_df: voucher data
        :return: dataframe: removed outlier df
        """
        quantile_filter = voucher_df['total_orders'].quantile(total_order_quantile_filter)
        voucher_df_filtered = voucher_df[voucher_df["total_orders"] < quantile_filter]
        return voucher_df_filtered

    def generate_customer_segments(self, voucher_df):
        """
        Generates customer segment for
        :param voucher_df:
        :return: dataframe: customer segmented data
        """
        voucher_df['frequent_segment'] = voucher_df['total_orders'].apply(get_frequency_segment)
        voucher_df['recency_segment'] = voucher_df['last_order_ts'].apply(get_recency_segment)
        voucher_df.dropna(axis=0, how='any', inplace=True)
        return voucher_df

    def voucher_generation(self, voucher_df):
        """
        Generates voucher per customer segment
        :param voucher_df: voucher dataframe
        :return: :dict, dict: vouchers generated for customer segments
        """
        voucher_df_country = voucher_df[voucher_df['country_code'] == country_code]

        self.voucher_amount_recency = voucher_df_country.groupby(['recency_segment'])['voucher_amount'].agg(
            pd.Series.mode).to_dict()

        self.voucher_amount_frequency = voucher_df_country.groupby(['frequent_segment'])['voucher_amount'].agg(
            pd.Series.mode).to_dict()
        return self.voucher_amount_recency, self.voucher_amount_frequency

    def saving_voucher_amount(self):
        """
        Saves generated voucher files per customer segment
        """
        with open(data_dir + '/' + voucher_amount_recency_file, 'w') as fr:
            json.dump(self.voucher_amount_recency, fr)
        with open(data_dir + '/' + voucher_amount_frequency_file, 'w') as ff:
            json.dump(self.voucher_amount_frequency, ff)
        logging.info('successfully saved the voucher amount')

    def run_pipeline(self):
        """
        Run entire etl pipeline to generate voucher as per customer segments
        """
        voucher_df = self.read_voucher_data()
        processed_voucher_df = self.pre_process_voucher_data(voucher_df)
        normalised_voucher_df = self.outlier_removal(processed_voucher_df)
        customer_segments_df = self.generate_customer_segments(normalised_voucher_df)
        self.voucher_generation(customer_segments_df)
        self.saving_voucher_amount()


if __name__ == "__main__":
    pipeline_obj = VoucherGeneration()
    pipeline_obj.run_pipeline()
