# APPLICATION URL CONSTANTS ###

PREFIX_APP_URL = '/customer-voucher'
PING_API_URL = "/ping"
VOUCHER_SELECTION_API_URL = ''

# Other global constants
SUCCESS_STATUS_STRING = 'success'
ERROR_STATUS_STRING = 'error'
