import dataclasses
import os
import sys
import json
from uuid import uuid4

sys.path.append('../../')

from src.service.voucher import VoucherGeneration
from fastapi import APIRouter, status, Response, Request
from src.dao.output import OutputDao
from config.main_config import data_dir, voucher_amount_recency_file, voucher_amount_frequency_file, logging
from src.constants.application_constants import PREFIX_APP_URL
from src.dao.input import InputDao, SegmentName
from utils.helper_functions import get_recency_segment, get_frequency_segment


def are_parameters_valid(input_data: dataclasses.dataclass()) -> bool:
    """
    Checks if API parameters are valid
    :param input_data: parameters from input query
    :return: bool
    """
    if (input_data.customer_id is None) | (input_data.country_code is None) | (input_data.last_order_ts is None) | (
            input_data.first_order_ts is None) | (
            input_data.total_orders is None):
        return False
    else:
        return True


"""
## Parameters:
* customer_id: id of the customer
* country_code: customer’s country ('Peru')
* last_order_ts: timestamp of the last order placed by a customer
* first_order_ts: timestamp f the first order placed by a customer
* total_orders: total orders placed by a customer
* segment_name: segment a customer belongs to ['recency_segment, frequent_segment]

## Raises:
* HTTPException: status_code=500 -> Undefined parameters, unable to read voucher amount files
* HTTPException: status_code=204 -> No voucher found for the customer
* HTTPException: status_code=422 -> Invalid API parameters data-type
* Success: status_code=200 (Ok) -> float: selected voucher amount for the customer

## Returns
* float: selected voucher amount for the customer
"""

# run etl pipeline to generate voucher per customer segment
pipeline_obj = VoucherGeneration()
pipeline_obj.run_pipeline()

try:
    with open(data_dir + '/' + voucher_amount_recency_file) as f:
        voucher_amount_recency = json.load(f)
    with open(data_dir + '/' + voucher_amount_frequency_file) as f:
        voucher_amount_frequency = json.load(f)
except Exception as e:
    logging.error("Error loading voucher amount file", e)
    logging.critical("Exiting due to {}".format(e))
    sys.exit(1)

router = APIRouter()


@router.get(PREFIX_APP_URL)
def serve(
        request: Request,
        response: Response,
        customer_id: int,
        country_code: str,
        last_order_ts: str,
        first_order_ts: str,
        total_orders: int,
        segment_name: SegmentName
):
    trace_id = uuid4().hex
    logging.info(f'Request received for customer id {customer_id}',
                 extra={'customer_id': customer_id, 'trace_id': trace_id})
    try:
        input_data: InputDao = InputDao(customer_id, country_code, last_order_ts, first_order_ts, total_orders,
                                        segment_name)
        logging.info(f'Request received for customer_id : {input_data.customer_id}', extra={'trace_id': trace_id})
        if are_parameters_valid(input_data):
            try:
                if input_data.segment_name == 'recency_segment':
                    voucher_amount = voucher_amount_recency.get(get_recency_segment(input_data.last_order_ts))
                else:
                    voucher_amount = voucher_amount_frequency.get(get_frequency_segment(input_data.total_orders))
            except Exception as e:
                logging.error(f'Error in processing data {e}', extra={'trace_id': trace_id})
                error_obj = OutputDao(0.0)
                response.status_code = status.HTTP_400_BAD_REQUEST
                response.headers["trace_id"] = trace_id
                return error_obj
            else:
                if voucher_amount:
                    return OutputDao(voucher_amount)
                else:
                    response.status_code = status.HTTP_204_NO_CONTENT
                    response.headers["trace_id"] = trace_id
                    return response
        else:
            logging.error(f"API parameters not correct",
                          extra={"trace_id": trace_id})
            response.status_code = status.HTTP_422_UNPROCESSABLE_ENTITY
            return response

    except Exception as e:
        logging.exception(e)
        error_obj = OutputDao(0.0)
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        response.headers["trace_id"] = trace_id
        return error_obj
