from fastapi import APIRouter
from datetime import datetime
from src.constants.application_constants import PING_API_URL

router = APIRouter()


# Route to check whether app is running or not
@router.get(PING_API_URL)
def ping():
    """
    ping route to quick check if application is running or not
    """
    return {'Voucher selection API is up and running at : {}'.format(datetime.now().date())}
