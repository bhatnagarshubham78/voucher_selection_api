import sys
from fastapi.testclient import TestClient
from datetime import datetime

sys.path.append('../')
from src.routes.ping import router
from src.constants.application_constants import PING_API_URL

client = TestClient(router)


def test_ping():
    response = client.get(PING_API_URL)
    assert response.status_code == 200
    assert response.json() == ['Voucher selection API is up and running at : {}'.format(datetime.now().date())]
