import time
import json
import sys
from locust import HttpUser, task, between

sys.path.append('../')

from src.constants.application_constants import PREFIX_APP_URL


class QuickstartUser(HttpUser):
    wait_time = between(1, 3)

    @task(1)
    def test_voucher_selection(self):
        input_data = {
            "customer_id": 2,
            "country_code": "Peru",
            "last_order_ts": "2018-05-03 00:00:00",
            "first_order_ts": "2017-05-03 00:00:00",
            "total_orders": 30,
            "segment_name": "frequent_segment"
        }
        self.client.get(PREFIX_APP_URL, params=input_data)
