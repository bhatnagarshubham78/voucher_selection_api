import sys
from fastapi.testclient import TestClient

sys.path.append('../')
from src.routes.voucher_selection import router
from src.constants.application_constants import PREFIX_APP_URL

client = TestClient(router)


def test_serve_valid():
    input_data = {
        "customer_id": 123,
        "country_code": "Peru",
        "last_order_ts": "2018-05-03 00:00:00",
        "first_order_ts": "2017-05-03 00:00:00",
        "total_orders": 15,
        "segment_name": "recency_segment"
    }
    test_data = {
        "voucher_amount": 2640.0
    }
    response = client.get(
        PREFIX_APP_URL, params=input_data)
    assert response.status_code == 200
    assert response.json() == test_data


def test_serve_str_cid():
    input_data = {
        "customer_id": '12',
        "country_code": "Peru",
        "last_order_ts": "2018-05-03 00:00:00",
        "first_order_ts": "2017-05-03 00:00:00",
        "total_orders": 15,
        "segment_name": "recency_segment"
    }
    test_data = {
        "voucher_amount": 2640.0
    }
    response = client.get(
        PREFIX_APP_URL, params=input_data)
    assert response.status_code == 200
    assert response.json() == test_data


def test_serve_no_content():
    input_data = {
        "customer_id": "12",
        "country_code": "Peru",
        "last_order_ts": "2018-05-03 00:00:00",
        "first_order_ts": "2017-05-03 00:00:00",
        "total_orders": 90,
        "segment_name": "frequent_segment"
    }
    response = client.get(
        PREFIX_APP_URL, params=input_data)
    assert response.status_code == 204
