import sys
from datetime import datetime, timezone

sys.path.append('../../')
from config.main_config import frequency_segment_dict


def str_to_timestamp(ts_str):
    """
    Converts timestamp from str to datetime format and current datetime
    :param ts_str: timestamp in str
    :return: timestamp in datetime, current datetime stamp
    """
    str_ts = None
    current_ts = None
    try:
        str_ts = datetime.strptime(ts_str, '%Y-%m-%d %H:%M:%S%z')
        current_ts = datetime.now(timezone.utc)
    except ValueError:
        str_ts = datetime.strptime(ts_str, '%Y-%m-%d %H:%M:%S')
        current_ts = datetime.now()
    finally:
        return str_ts, current_ts


def get_recency_segment(last_order_ts):
    """
    Get recency segment for a customer based on last order timestamp
    :param last_order_ts: str: last order timestamp
    :return: str: recency segment
    """
    if isinstance(last_order_ts, str):
        last_order_ts, current_ts = str_to_timestamp(last_order_ts)
    else:
        current_ts = datetime.now(timezone.utc)
    days_from_last_order = (current_ts - last_order_ts).days
    recency_seg = None
    if days_from_last_order in range(30, 61):
        recency_seg = "30-60"
    elif days_from_last_order in range(61, 91):
        recency_seg = "61-90"
    elif days_from_last_order in range(91, 121):
        recency_seg = "91-120"
    elif days_from_last_order in range(121, 181):
        recency_seg = "121-180"
    elif days_from_last_order > 180:
        recency_seg = "180+"

    return recency_seg


def get_frequency_segment(total_orders):
    """
    Get frequency segment for a customer based on total orders
    :param total_orders: int: total orders count
    :return: str: frequency segment
    """
    try:
        freq_seg = [frequency_segment_dict[key] for key in frequency_segment_dict if total_orders in key][0]
    except IndexError:
        return None
    else:
        return freq_seg


